package uk.org.toledo.testsupport.utils

import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.ui.WebDriverWait
import java.util.function.Function


class WaitingSupport {


    /**
     * Wait webdriver until given closure returns true, or timeout is reached. Typically,
     * the closure will check for the visibility of a web element defined in delegate class.
     * If timeout is reached before until returns true then methods throws a TimeoutException.
     * @param driver Webdriver instance to wait
     * @param until closure which delegates to a class which implements Drivable
     * @param timeout number of seconds to wait before timing out
     * @return void
     */
    static waitDriver(WebDriver driver, Closure until){
        Function isTrue = new Function<WebDriver, Boolean>() {
            @Override
            Boolean apply(WebDriver d) {
                return until()      //wait until returns true - nb closure can access driver and methods in delegate class!
            }
        }
        new WebDriverWait(driver, 3).until(isTrue)
    }


}
