package uk.org.toledo.testsupport.templates

import org.openqa.selenium.WebDriver
import uk.org.toledo.testsupport.traits.*

/**
 * An example page object configuration which models a browser page. This configuration implements the loadable trait,
 * which means any subclass must declare an isLoaded method.
 *
 * To create a new page instance, call the create factory method below which creates the concrete page instance,
 * sets the driver object and waits for the page to load.
 */
abstract class Page implements Drivable, Loadable {

    /**
     * Create a new instance of pageClass which subclasses Page,
     * set driver on new instance and wait to load.
     * @param pageClass concrete subclass of Page which we want to initialise
     * @param driver WebDriver instance
     * @return an initialised instance of pageClass
     */
    static create(Class<Page> pageClass, WebDriver driver){
        pageClass
                .newInstance()
                .setDriver(driver)
                .load()

    }

}
