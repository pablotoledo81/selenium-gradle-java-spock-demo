package uk.org.toledo.testsupport.templates

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import uk.org.toledo.testsupport.traits.*

/**
 * An example Iframe component configuration which models a component which contains content in an Iframe.
 * This configuration implements the loadable trait, which means any subclass must declare an isLoaded method.
 *
 * To create a new iFrame component instance, call the create factory method below which creates the concrete
 * iFrame component instance, switches into the iFrame and waits for the component to load.
 */
abstract class IframeComponent implements Drivable, Loadable, Iframe {

    /**
     * Create a new instance of iframeComponent which subclasses Page,
     * set driver on new instance and wait to load.
     * @param iframeComponent concrete subclass of Page which we want to instantiate
     * @param driver WebDriver instance
     * @param iframeLocator By locator which should be used to find the iFrame
     * @return an initialised instance of iframeComponent
     */
    static create(Class<IframeComponent> iframeComponent, WebDriver driver, By iframeLocator){
        iframeComponent
                .newInstance()
                .setDriver(driver)
                .setIframeLocator(iframeLocator)
                .enterIframe()
                .load()

    }

}
