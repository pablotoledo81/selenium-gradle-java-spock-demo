package uk.org.toledo.testsupport.templates

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import uk.org.toledo.testsupport.traits.*

/**
 * An example child component object configuration which models a component which is 'parented' by another component.
 * A typical use case is a list of search results on a search page where each result needs to be modelled as an object.
 * If a child component needs to wait on a condition before it can be interacted with, modify this configuration to
 * implement the Loadable trait.
 *
 * To create a new child component instance, call the create factory method below which creates the concrete
 * child component instance.
 */
abstract class ChildComponent implements Drivable, Parentable {

    /**
     * Create and initialise a new instance of childComponentClass which subclasses ChildComponent.
     * @param childComponentClass concrete subclass of ChildComponent which we want to initialise
     * @param driver WebDriver instance
     * @param parent WebElement which parents this instance
     * @return an initialised instance of childComponentClass
     */
    static create(Class<ChildComponent> childComponentClass, WebDriver driver, WebElement parent){
        childComponentClass
                .newInstance()
                .setDriver(driver)
                .setParent(parent)

    }

}
