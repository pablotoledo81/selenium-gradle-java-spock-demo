package uk.org.toledo.testsupport.templates

import org.openqa.selenium.WebDriver
import uk.org.toledo.testsupport.traits.*

/**
 * An example component object configuration which models a component on a browser page (e.g. menu, popup).
 * This configuration implements the loadable trait, which means any component subclass must declare an isLoaded method.
 *
 * To create a new component instance, call the create factory method below which creates the concrete component instance
 * and waits for the component to load.
 */
abstract class Component implements Drivable, Loadable, Unloadable {

    /**
     * Create and initialise a new instance of componentClass which subclasses Component.
     * @param componentClass concrete subclass of Component which we want to initialise
     * @param driver WebDriver instance
     * @return an initialised instance of componentClass
     */
    static create(Class<Component> componentClass, WebDriver driver){
        componentClass
                .newInstance()
                .setDriver(driver)
                .load()

    }

}
