package uk.org.toledo.testsupport.traits

import groovy.transform.SelfType

/**
 * Implement this trait for classes where we want to wait until the class instance has 'loaded' before
 * interacting with it. See the Page and Component templates for example usages of this trait.
 */
@SelfType(Drivable)
trait Loadable {

    /**
     * Method which should return true when the implementing class instance is deemed
     * to have finished loading and can be safely interacted with.
     * @return true when implementing class instance is loaded and can be safely
     * interacted with, else false
     */
    abstract boolean isLoaded()

    /**
     * Waits on the isLoaded() method to return true, or times out in X seconds
     * @return this class instance
     */
    Loadable load(){
        waitUntil { isLoaded() }
        return this
    }


}