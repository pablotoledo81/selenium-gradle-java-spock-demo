package uk.org.toledo.testsupport.traits

import groovy.transform.SelfType

/**
 * Implement this trait for classes where we want to wait until the class instance has 'unloaded'
 * before continuing test execution. See the Component template for example usage of this trait.
 */
@SelfType(Drivable)
trait Unloadable {

    /**
     * Method which should return true when the implementing class instance is
     * deemed to have finished unloading, and the tests can be safely continue.
     * @return true when implementing class instance is loaded and can be safely
     * interacted with, else false
     */
    abstract boolean isUnloaded()

    /**
     * Waits on the isUnloaded() method to return true, or times out in X seconds
     * @return this class instance
     */
    Unloadable unload(){
        waitUntil { isUnloaded() }
        return this
    }


}