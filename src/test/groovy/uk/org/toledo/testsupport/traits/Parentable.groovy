package uk.org.toledo.testsupport.traits

import groovy.transform.SelfType
import org.openqa.selenium.WebElement

/**
 * Implement this trait for classes which are logical 'children' of another class
 * (e.g. Search results on a search page.). See ChildComponent template for example
 * usage of this trait.
 */
@SelfType(Drivable)
trait Parentable {

    private WebElement parent

    /**
     * Get parent web element of this class instance
     * @return
     */
    WebElement getParent(){
        return this.parent
    }

    /**
     * Set parent web element for this class instance
     * @param parent
     * @return this class instance
     */
    Parentable setParent(WebElement parent){
        this.parent = parent
        return this
    }

}