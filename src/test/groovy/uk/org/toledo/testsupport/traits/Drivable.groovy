package uk.org.toledo.testsupport.traits

import org.openqa.selenium.By
import org.openqa.selenium.NoSuchElementException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import uk.org.toledo.testsupport.utils.WaitingSupport

/**
 * Implement this trait to give a class access to a webdriver instance, and various convenience methods
 * which facilitate common automation tasks such as waiting or checking that an element exists.
 * All other automation traits require this trait to be implemented.
 */
trait Drivable {

    private WebDriver driver

    /**
     * Get the driver instance for this class instance
     * @return WebDriver instance for this class instance
     */
    WebDriver getDriver(){
        return this.driver
    }

    /**
     * Set the driver for this class instance
     * @param driver WebDriver instance to use for this class instance
     * @return this class instance
     */
    Drivable setDriver(WebDriver driver){
        this.driver = driver
        return this
    }

    /**
     * Wait the driver for this class instance until the given closure
     * returns true, or timeout is reached.
     * @param until closure which should return true
     * @return this class instance
     */
    Drivable waitUntil(Closure until){
        WaitingSupport.waitDriver(driver, until)
        return this
    }

    /**
     * Gets list of web elements which match given by selector. This method
     * provides a shortcut to the native driver.findElements method.
     * @param by selector on which web elements should be matched
     * @return list of matching web elements (which may be empty)
     */
    List<WebElement> getElements(By by){
        return driver.findElements(by)
    }

    /**
     * Gets first web element which matches given by selector. This method
     * provides a shortcut to the native driver.findElement method, but swallows
     * the NoSuchElementFound exception and instead returns an null result.
     * This is useful if we want to find an element with a by expression which
     * may/may not return a web element but where we don't want a NoSuchElementException
     * thrown e.g. result = findElement(by).
     * @param by
     * @return first matching web element, or null if no matching element found
     */
    WebElement getElement(By by){
        WebElement element = null
        try {
            element = driver.findElement(by)
        }
        catch (NoSuchElementException e){
            println("element $by not found.")
        }
        return element
    }

    /**
     * Get the first matching web element returned by executing the given closure.
     * If the closure throws a NoSuchElementException this is swallowed, and a null
     * result is returned instead. This is useful if we want to evaluate an expression
     * which may/may not return a web element but where we don't want a NoSuchElementException
     * thrown e.g. result = someElement.findElement(by).
     * @param closure which should return a web element, but may throw a NoSuchElementException
     * @return web element, or null
     */
    WebElement getElement(Closure closure){
        WebElement element = null
        try {
            element = closure() as WebElement
        }
        catch (NoSuchElementException e){
            println("element not found by closure expression ${e.stackTrace}.")
        }
        return element
    }

    /**
     * Return true if element exists for given by locator, else return false
     * @param by
     * @return true if element exists for given by, else return false
     */
    boolean elementExists(By by){
        return getElements(by) as boolean
    }

    /**
     * Move to given web element
     * @param element
     * @return this class instance
     */
    Drivable moveToElement(WebElement element){
        new Actions(driver)
            .moveToElement(element)
            .perform()
        return this
    }



}