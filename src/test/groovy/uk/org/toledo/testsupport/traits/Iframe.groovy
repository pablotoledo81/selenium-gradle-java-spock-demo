package uk.org.toledo.testsupport.traits

import groovy.transform.SelfType
import org.openqa.selenium.By
import org.openqa.selenium.WebElement

/**
 * Implement this trait to give a class access to iFrame content and convenience methods
 * to enter/exit the iFrame. See the IframeComponent template for example usage of this trait.
 */
@SelfType(Drivable)
trait Iframe {
    private By iframeLocator

    /**
     * Set the iframeLocator for this instance
     * @param iframeRoot
     * @return this class instance
     */
    Iframe setIframeLocator(By iframeRoot){
        this.iframeLocator = iframeRoot
        return this
    }

    /**
     * Switch driver to this instance's iframe
     * @return this class instance
     */
    Iframe enterIframe(){
        WebElement iframeRoot = driver.findElement(iframeLocator)
        driver.switchTo().frame(iframeRoot)
        return this
    }

    /**
     * Switch driver back to default content
     */
    void exitIframe(){
        driver.switchTo().defaultContent()
    }


}
