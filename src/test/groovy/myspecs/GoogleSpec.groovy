package myspecs

import org.openqa.selenium.WebDriver
import io.github.bonigarcia.wdm.WebDriverManager
import org.openqa.selenium.chrome.ChromeDriver
import pages.GooglePage
import spock.lang.Shared
import spock.lang.Specification
import uk.org.toledo.testsupport.templates.Page

class GoogleSpec extends Specification {

    @Shared GooglePage googlePage
    @Shared WebDriver driver

    def setupSpec(){
        println("Create driver")
        WebDriverManager.chromedriver().setup()
        driver = new ChromeDriver()
    }

    def cleanupSpec(){
        println("Quit driver")
        googlePage.driver.quit()
    }


    def "I can enter a partial search term in google and get predicted search terms"() {
        given: "I'm at the google homepage"
        GooglePage.go(driver)
        googlePage = Page.create(GooglePage, driver) as GooglePage

        expect: "the page is loaded"
        googlePage.isLoaded()

        when: "I enter a search term and get the search suggestions"
        def searchSuggestions = googlePage
                .enterSearchTerm('selenium webdriver')
                .getSearchSuggestionsPane()
                .getSearchSuggestions()

        then: "I get some search suggestions"
        searchSuggestions.size() > 0

        and: "Each suggestion includes the phrase 'selenium webdriver"
        searchSuggestions.every{ it.suggestionText.contains('selenium webdriver') }

        and: "Each suggestion except the first has a bold word"
        searchSuggestions.drop(1).every{ it.boldWordText }

        and: "Each result is highlighted when I hover over it"
        searchSuggestions.every{
            it.moveToElement(it.parent)
            it.isHighlighted()
        }

        cleanup: "close search suggestions pane and wait "
        googlePage
                .getSearchSuggestionsPane()
                .close()

    }

    def "I can open the app widget and interact with the app tiles"() {
        when: "I open the app widget and get the app tiles"
        def appTiles = googlePage
                .openAppWidget()
                .getAppTiles()

        then: "Each app tile has a name"
        appTiles.every { it.appName }

        and: "I can scroll to each app tile in turn"
        appTiles.every{
            it.moveToElement(it.parent)
        }

        cleanup: "I close the app widget"
        googlePage.closeAppWidget()

    }



}