package pages

import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import uk.org.toledo.testsupport.templates.*

class AppTile extends ChildComponent {

    private static final APP_NAME = By.cssSelector('span.Rq5Gcb')

    private WebElement getAppNameSpan(){
        return parent.findElement(APP_NAME)
    }

    String getAppName(){
        println("App name: $appNameSpan.text")
        return appNameSpan.text
    }


}
