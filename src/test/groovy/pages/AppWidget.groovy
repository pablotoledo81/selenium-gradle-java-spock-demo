package pages

import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import uk.org.toledo.testsupport.templates.*

class AppWidget extends IframeComponent {

    private static final APP_ICON = By.cssSelector('li.j1ei8c')

    boolean isLoaded(){
        List<WebElement> icons = driver.findElements(APP_ICON)
        println("${icons.size()} icons")
        return icons.every{ it.displayed }
    }

    boolean isUnloaded(){
        return !isLoaded()
    }

    private List<WebElement> getAppIcons(){
        return driver.findElements(APP_ICON)
    }

    List<AppTile> getAppTiles(){
        println("${appIcons.size()} apps found")
        appIcons.collect{ parent->
            ChildComponent.create(AppTile, driver, parent)
        } as List<AppTile>
    }


}
