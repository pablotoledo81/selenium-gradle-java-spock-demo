package pages

import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.pagefactory.ByChained
import uk.org.toledo.testsupport.templates.ChildComponent


class SearchSuggestion extends ChildComponent {

    private static final By OPTION_DIV = By.className('sbtc')
    private static final By SUGGESTION = new ByChained(OPTION_DIV, By.tagName('span'))
    private static final By BOLD_WORD = new ByChained(SUGGESTION, By.tagName('b'))

    private WebElement getSuggestion(){
        return getElement{ parent.findElement(SUGGESTION) }
    }

    private WebElement getBoldWord(){
        return getElement{ parent.findElement(BOLD_WORD) }
    }

    String getSuggestionText(){
        String text = suggestion.text
        println("search suggestion: $text")
        return text
    }

    String getBoldWordText(){
        String word = boldWord ? boldWord.text : ''
        println("bold word: $word")
        return word
    }

    boolean isHighlighted(){
        return parent.getAttribute('class').contains('sbhl')
    }


}
