package pages

import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebElement
import uk.org.toledo.testsupport.templates.ChildComponent
import uk.org.toledo.testsupport.templates.Component

class SearchSuggestionsPanel extends Component {

    private static final SEARCH_FIELD = By.name('q')
    private static final SUGGESTIONS_PANEL = By.cssSelector('ul.erkvQe')
    private static final SUGGESTIONS = By.className('sbct')

    boolean isLoaded(){
        return suggestionsPanel.displayed
    }

    boolean isUnloaded(){
        return !suggestionsPanel.displayed
    }

    private WebElement getSuggestionsPanel(){
        return driver.findElement(SUGGESTIONS_PANEL)
    }

    private List<WebElement> getSuggestions(){
        return suggestionsPanel.findElements(SUGGESTIONS)
    }

    List<SearchSuggestion> getSearchSuggestions(){
        println("${suggestions.size()} suggestions found")
        suggestions.collect{ parent->
            ChildComponent.create(SearchSuggestion, driver, parent)
        } as List<SearchSuggestion>
    }

    void close(){
        driver.findElement(SEARCH_FIELD).sendKeys(Keys.ESCAPE)
        unload()
    }

}
