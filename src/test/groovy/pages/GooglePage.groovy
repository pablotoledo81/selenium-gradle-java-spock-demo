package pages

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import uk.org.toledo.testsupport.templates.*
import uk.org.toledo.testsupport.traits.Iframe

class GooglePage extends Page implements Iframe {

    private static final By APP_WIDGET_LINK = By.cssSelector('a.gb_D')
    private static final By IFRAME_ROOT = By.xpath('//iframe[@tabindex=0]')
    private static final By SEARCH_FIELD = By.name('q')

    static go(WebDriver driver){
        driver.get('https://www.google.co.uk')
    }

    boolean isLoaded(){
        return driver.title == 'Google'
    }

    private WebElement searchField(){
        return driver.findElement(SEARCH_FIELD)
    }

    GooglePage enterSearchTerm(String searchTerm) {
        searchField().sendKeys(searchTerm)
        return this
    }

    SearchSuggestionsPanel getSearchSuggestionsPane(){
        Component.create(SearchSuggestionsPanel, driver) as SearchSuggestionsPanel
    }

    AppWidget openAppWidget(){
        driver.findElement(APP_WIDGET_LINK).click()
        return IframeComponent.create(AppWidget, driver, IFRAME_ROOT) as AppWidget
    }

    GooglePage closeAppWidget(){
        exitIframe()
        driver.findElement(APP_WIDGET_LINK).click()
        return this
    }



}
