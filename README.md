# Introduction #
This is a demo of a light-weight browser automation stack using selenium with groovy, spock and gradle. This demo deliberately eliminates any automation frameworks which wrap selenium, and uses the fewest possible libaries necessary to get the job done. Such libraries are convenient, but come at the cost of flexibility and ironically can result in code which is more (not less) complex as you find yourself working around the limitations of your chosen framework.

The motivation behind this demo come up with a design pattern which allows for maximum flexibilty, minimum use of libraries and 'black magic, while reducing boiler plate code as much as possible.

This demo uses a web component model (an extension of the page object model), which is designed to better cater for automation of javascript-rich sites build using frameworks such as vue.js or react.

In the page object model, web pages are modelled as classes and provide encapsulation of the page state and behaviours. In the component model, web components (such as menus, popups etc.) are modelled in a similar way. The component model is especially useful where your web components have complex behaviours and dynamically loaded content.

In these situtations we typically have to wait for a component to finish loading before we can safely interact with it. We may also need to wait for a component to unload before returning to the previous context (e.g. modal popups). Automating these types of websites can be hard using the traditional page object model, often resulting in the dreaded 'Stale element exception'.

This demo uses composition instead traditional inheritance to build appropriate page / component classes. This accomodates of the fact that every site presents different automation challenges, and there is no 'one size fits all' solution.

This demo borrows many ideas from the excellent [geb](https://gebish.org/) project, as well as ideas such as using [composition](https://www.awesome-testing.com/2019/03/selenium-page-object-pattern-how-to.html), [Fluent-style page objects](https://www.vinsguru.com/selenium-webdriver-how-to-design-page-objects-in-fluent-style/) and [Factory patterns](https://www.vinsguru.com/selenium-webdriver-design-patterns-in-test-automation-factory-pattern/) in automation. I'd also like to thank my brilliant colleagues for all their help and input in developing this demo.

Feel free to clone, improve and share. Happy automating!


# Dependencies
Java 8 JRE
Google chrome

# To Run on mac/linux#
navigate to the project root in your terminal and run
`./gradle test`


# To Run on windows#
navigate to the project root in your command prompt and run
`gradle.bat test`